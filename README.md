# ContactUs

## 概要

Zendeskを利用したお問い合わせ送信機能です。 
CSへ送信するお問い合わせフォーマットを共通化することで、CS対応の負荷軽減を目的としています。 

※ ZendeskのUnitySDKの導入が必要となります。 

※ 各項目を入力するためのUIに関しては各プロジェクトで実装して下さい。

## 使用手順

### 1. ZendeskのUnitySDKをインポートします。
#### Unityから追加する場合
追加方法はPackage Managerから`Add package from git url...`を選んで
`https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/zendesk.git?path=Packages/Zendesk#1.2.4`を入力します。

#### minifest.jsonを編集して追加する場合
manifest.jsonのdependenciesに`"com.zendesk.support": "https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/zendesk.git?path=Packages/Zendesk#1.2.4"`を追加します。

### 2. ContactUsのパッケージを打つ活かします。
#### Unityから追加する場合
追加方法はPackage Managerから`Add package from git url...`を選んで
`https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/contactus.git?path=Packages/ContactUs#1.0.0`を入力します。

#### minifest.jsonを編集して追加する場合
manifest.jsonのdependenciesに`"jp.co.ponos.contact-us": "com.zendesk.support": "https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/contactus.git?path=Packages/ContactUs#1.0.0"`を追加します。

### 3. HierarchyにGameObjectを新規作成し、ContactUsRequesterコンポーネントを追加します。
	
* ZendeskCore、ZendeskAuthHandler、ZendeskSupportProviderのコンポーネントも自動で追加されます。

### 4. ContactUsRequesterコンポーネントに、Zendeskへ接続するためのパラメータを設定します。
	
| パラメータ名 | 説明 |
| :-- | :-- |
| **ZendeskUrl** | Zendeskへの接続に必要なURLです。<br>管理画面内「モバイルSDK」の「Zendesk URL」を参照してください。|
| **AppId** | Zendeskでアプリの識別に必要なアプリのIDです。<br>管理画面内「モバイルSDK」の「App ID」を参照してください。 |
| **ClientId** | Zendeskでアプリの識別に必要なアプリのIDです。<br>管理画面内「モバイルSDK」の「Client ID」を参照してください。 |

### 5. ContactUsRequester.Initialize()メソッドで問い合わせを初期化します。
	
```cs
/// <summary>
/// 初期化する。
/// </summary>
/// <param name="appTitle">アプリタイトル。</param>
/// <param name="locale">サポートのロケール。</param>
public void Initialize(string appTitle, string locale)
```

| 引数 | 説明 |
| :-- | :-- |
| **appTitle** | Zendesk上でアプリ名として表示される文字列。 |
| **locale** | サポート対象のロケール。 |

### 6. ContactUsRequesterに問い合わせの各項目のプロパティを設定します。

| プロパティ名 | 説明 |
| :-- | :-- |
| **UserId** | ユーザーID。<br>アプリごとにユーザーを識別するための情報。 |
| **UserName** | ユーザー名。 |
| **PlatformVersion** | アプリを実行しているプラットフォームのバージョン文字列。 |
| **DeviceName** | アプリを実行している機種名。 |
| **Version** | アプリのバージョン文字列。 |
| **AdditionalInformation** | 問い合わせに関する追加情報。<br>アプリごとに自由に内容を指定できる。 |

### 7. ContactUsRequester.SendRequest()メソッドで問い合わせを送信します。

```cs
/// <summary>
/// リクエストを送信する。
/// </summary>
/// <param name="requestType">問い合わせ種別。</param>
/// <param name="platform">問い合わせをしているプラットフォーム。</param>
/// <param name="emailAddress">連絡用のメールアドレス。</param>
/// <param name="commentBody">問い合わせの本文。</param>
public void SendRequest(RequestType requestType, RequesterPlatform platform, string emailAddress, string commentBody)
```

| 引数 | 説明 |
| :-- | :-- |
| **requestType** | 問い合わせの種別。<br><br>**RequestType.Question** ... 操作や仕様質問。<br>**RequestType.Bug** ... 不具合連絡。<br>**RequestType.Purchasing** ... 課金に関すること。<br>**RequestType.Request** ... 要望。<br>**RequestType.Other** ... その他。 |
| **platform** | 問い合わせをしているアプリのプラットフォーム。<br><br>**RequesterPlatform.iOS** ... iOS。<br>**RequesterPlatform.Android** ... Android。 |
| **emailAddress** | 連絡用のメールアドレス。 |
| **commentBody** | 問い合わせの本文。 |

## バージョン履歴

### ver.1.0.0
* 最初のバージョン。
* WTプロジェクトから該当機能を分離して共通ライブラリ化。
