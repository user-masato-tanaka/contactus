using UnityEngine;

namespace Ponos.CommonUnity.ContactUs
{
    /// <summary>
    /// 問い合わせのログを出力するためのクラス。
    /// </summary>
    public static class ContactUsLogger
    {
        /// <summary>
        /// ログの出力を有効化する場合、true。
        /// </summary>
        public static bool Enabled { get; set; } = true;

        /// <summary>
        /// 通常のログを出力する。
        /// </summary>
        /// <param name="message">ログのメッセージ。</param>
        public static void Log(string message)
        {
            if(!Enabled)
            {
                return;
            }

            Debug.Log($"<color=#a0522d><b>[ ContactUs ]</b></color> {message}");
        }

        /// <summary>
        /// エラーのログを出力する。
        /// </summary>
        /// <param name="message">ログのメッセージ。</param>
        public static void LogError(string message)
        {
            if(!Enabled)
            {
                return;
            }
            
            Debug.LogError($"<color=#a0522d><b>[ ContactUs ]</b></color> {message}");
        }
    }
}