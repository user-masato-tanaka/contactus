using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Zendesk.Common;
using Zendesk.Providers;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.Support;
using Zendesk.Internal.Models.HelpCenter;

namespace Ponos.CommonUnity.ContactUs
{
    /// <summary>
    /// 問い合わせ送信クラス。
    /// 
    /// ZendeskのAPIを用いて問い合わせ内容を送信するため、ZendeskSDKのインポートが必要であり、
    /// ZendeskCore、ZendeskAuthHandler、ZendeskSupportProviderのコンポーネントを利用する。
    /// 
    /// PONOSのCSチームが全プロジェクト共通で問い合わせ時に必要としている項目を設定可能。
    ///  - 問合せ種類: 62396628
    ///  - 説明: 51389648
    ///  - ユーザーID: 66450628
    ///  - ユーザー名: 80799448
    ///  - プラットフォーム: 61953727
    ///  - プラットフォームバージョン: 66380848
    ///  - 機種名: 61977007
    ///  - アプリバージョン: 61870527
    ///  - 件名: 51389628
    ///  - アプリタイトル: 61955647
    ///  - 追加項目: 900009553746
    /// </summary>
    [RequireComponent(typeof(ZendeskCore), typeof(ZendeskAuthHandler), typeof(ZendeskSupportProvider))]
    public class ContactUsRequester : MonoBehaviour
    {
        /// <summary>
        /// 問い合わせ種別。
        /// </summary>
        public enum RequestType
        {
            /// <summary>
            /// 操作や仕様質問。
            /// </summary>
            Question = 0,
            /// <summary>
            /// 不具合連絡。
            /// </summary>
            Bug,
            /// <summary>
            /// 課金に関すること。
            /// </summary>
            Purchasing,
            /// <summary>
            /// 要望。
            /// </summary>
            Request,
            /// <summary>
            /// その他。
            /// </summary>
            Other
        }

        /// <summary>
        /// 問い合わせをしているプラットフォーム種別。
        /// </summary>
        public enum RequesterPlatform
        {
            /// <summary>
            /// iOS。
            /// </summary>
            iOS = 0,
            /// <summary>
            /// Android。
            /// </summary>
            Android
        }

        /// <summary>
        /// 「問合せ種類」のチケットフィールドID。
        /// </summary>
        const long RequestTypeFieldId = 62396628;

        /// <summary>
        /// 「ユーザーID」のチケットフィールドID。
        /// </summary>
        const long UserIdFieldId = 66450628;

        /// <summary>
        /// 「ユーザー名」のチケットフィールドID。
        /// </summary>
        const long UserNameFieldId = 80799448;

        /// <summary>
        /// 「プラットフォーム」のチケットフィールドID。
        /// </summary>
        const long PlatformFieldId = 61953727;

        /// <summary>
        /// プラットフォームバージョン」のフィールドID。
        /// </summary>
        const long PlatformVersionFieldId = 66380848;

        /// <summary>
        /// 「機種名」のフィールドID。
        /// </summary>
        const long DeviceNameFieldId = 61977007;

        /// <summary>
        /// 「アプリバージョン」のフィールドID。
        /// </summary>
        const long ClientVersionFieldId = 61870527;

        /// <summary>
        /// 「アプリタイトル」のフィールドID。
        /// </summary>
        const long AppTitleFieldId = 61955647;

        /// <summary>
        /// 「追加項目」のフィールドID。
        /// </summary>
        const long DetailFieldId = 900009553746;

        /// <summary>
        /// 問い合わせ送信の準備が整っている場合、true。
        /// </summary>
        public bool IsReady { get; private set; } = false;

        /// <summary>
        /// サポートのロケール。
        /// </summary>
        string locale = string.Empty;

        /// <summary>
        /// ブランドを示すタグ。
        /// </summary>
        string brandTag = string.Empty;

        /// <summary>
        /// アプリタイトル。
        /// </summary>
        string appTitle = string.Empty;

        /// <summary>
        /// 問い合せ種類。
        /// </summary>
        RequestType requestType;

        /// <summary>
        /// プラットフォーム。
        /// </summary>
        RequesterPlatform platform;

        /// <summary>
        /// ユーザーID。
        /// ユーザーを識別するための情報。
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// ユーザー名。
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// プラットフォームバージョン。
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// 機種名。
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// アプリバージョン。
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 追加情報。
        /// </summary>
        public string AdditionalInformation { get; set; }

        [Header("Zendesk Settings")]

        /// <summary>
        /// Zendeskへの接続に必要なURL。
        /// 管理画面内「モバイルSDK」の「Zendesk URL」を参照。
        /// </summary>
        [SerializeField]
        string zendeskUrl = string.Empty;

        /// <summary>
        /// Zendeskでアプリの識別に必要なアプリのID。
        /// 管理画面内「モバイルSDK」の「App ID」を参照。
        /// </summary>
        [SerializeField]
        string appId = string.Empty;

        /// <summary>
        /// Zendeskでアプリの識別に必要なクライアントID。
        /// 管理画面内「モバイルSDK」の「Client ID」を参照。
        /// </summary>
        [SerializeField]
        string clientId = string.Empty;

        /// <summary>
        /// Zendeskのコア機能へのアクセス。
        /// </summary>
        ZendeskCore core = default;

        /// <summary>
        /// Zendeskのサポート送信のプロバイダ。
        /// </summary>
        ZendeskSupportProvider supportProvider = default;

        /// <summary>
        /// 初期化処理に成功した時に呼び出されるイベント。
        /// </summary>
        public event Action OnInitializeSucceeded = delegate { };

        /// <summary>
        /// 初期化処理に失敗した時に呼び出されるイベント。
        /// </summary>
        public event Action OnInitializeFailed = delegate { };

        /// <summary>
        /// 問い合わせリクエストに成功した時に呼び出されるイベント。
        /// </summary>
        public event Action OnRequestSucceeded = delegate { };

        /// <summary>
        /// 問い合わせリクエストに失敗した時に呼び出されるイベント。
        /// </summary>
        public event Action OnRequestFailed = delegate { };

        /// <summary>
        /// 初期化する。
        /// </summary>
        /// <param name="appTitle">アプリタイトル。</param>
        /// <param name="locale">サポートのロケール。</param>
        public void Initialize(string appTitle, string locale)
        {
            this.appTitle = appTitle;
            this.locale = locale;

            requestType = RequestType.Question;
            UserId = string.Empty;
            UserName = string.Empty;
#if UNITY_IOS
            platform = RequesterPlatform.iOS;
#elif UNITY_ANDROID
            platform = RequesterPlatform.Android;
#endif
            PlatformVersion = SystemInfo.operatingSystem;
            DeviceName = SystemInfo.deviceModel;
            Version = Application.version;
            AdditionalInformation = string.Empty;

            core = GetComponent<ZendeskCore>();
            supportProvider = GetComponent<ZendeskSupportProvider>();

            StartCoroutine(core.InitializeZendeskCore(OnInitialized, zendeskUrl, appId, clientId, locale));
        }

        /// <summary>
        /// Zendeskの初期化後の処理。
        /// </summary>
        /// <param name="response">初期化処理のレスポンス。</param>
        void OnInitialized(ZendeskResponse<ZendeskSettings> response)
        {
            if (response.IsError)
            {
                ContactUsLogger.LogError($"Initialize Failed. ( {response.ErrorResponse.Reason} )");

                IsReady = false;
                OnInitializeFailed?.Invoke();
            }
            else
            {
                ZendeskLocalStorage.LoadSupportStorage();

                var authHandler = GetComponent<ZendeskAuthHandler>();
                authHandler.InitializeZendeskAuth(core, locale);

                supportProvider.Initialize(authHandler, core, response.Result, null);

                ContactUsLogger.Log("Initialize Succeeded !");

                IsReady = true;
                OnInitializeSucceeded?.Invoke();
            }
        }

        /// <summary>
        /// カスタムフィールドの値リストを作成する。
        /// </summary>
        /// <returns>カスタムフィールドの値リスト。</returns>
        List<CustomField> CreateCustomFieldValues()
        {
            var list = new List<CustomField>();
            CustomField customField;

            // 問い合わせ種類。
            customField = new CustomField();
            customField.Id = RequestTypeFieldId;
            customField.Value = GetRequestTypeTag(requestType);
            list.Add(customField);

            // ユーザーID。
            if (!string.IsNullOrEmpty(UserId))
            {
                customField = new CustomField();
                customField.Id = UserIdFieldId;
                customField.Value = UserId;
                list.Add(customField);
            }

            // ユーザー名。
            if (!string.IsNullOrEmpty(UserName))
            {
                customField = new CustomField();
                customField.Id = UserNameFieldId;
                customField.Value = UserName;
                list.Add(customField);
            }

            // プラットフォーム。
            customField = new CustomField();
            customField.Id = PlatformFieldId;
            switch (platform)
            {
                case RequesterPlatform.iOS:
                    customField.Value = "iOS";
                    break;
                case RequesterPlatform.Android:
                    customField.Value = "Android";
                    break;
            }
            list.Add(customField);

            // プラットフォームバージョン。
            if (!string.IsNullOrEmpty(PlatformVersion))
            {
                customField = new CustomField();
                customField.Id = PlatformVersionFieldId;
                customField.Value = PlatformVersion;
                list.Add(customField);
            }

            // 機種名。
            if (!string.IsNullOrEmpty(DeviceName))
            {
                customField = new CustomField();
                customField.Id = DeviceNameFieldId;
                customField.Value = DeviceName;
                list.Add(customField);
            }

            // アプリバージョン。
            if (!string.IsNullOrEmpty(DeviceName))
            {
                customField = new CustomField();
                customField.Id = ClientVersionFieldId;
                customField.Value = Application.version;
                list.Add(customField);
            }

            // アプリタイトル。
            customField = new CustomField();
            customField.Id = AppTitleFieldId;
            customField.Value = appTitle;
            list.Add(customField);

            // 追加項目。
            customField = new CustomField();
            customField.Id = DetailFieldId;
            var stringBuilder = new StringBuilder();
#if UNITY_IOS
            stringBuilder.AppendLine("プラットフォーム:" + RequesterPlatform.iOS.ToString());
#elif UNITY_ANDROID
            stringBuilder.AppendLine("プラットフォーム:" + RequesterPlatform.Android.ToString());
#endif
            stringBuilder.AppendLine("プラットフォームバージョン:" + SystemInfo.operatingSystem);
            stringBuilder.AppendLine("機種名:" + SystemInfo.deviceModel);
            stringBuilder.AppendLine("アプリバージョン:" + Application.version);
            stringBuilder.AppendLine(AdditionalInformation);
            customField.Value = stringBuilder.ToString();
            list.Add(customField);

            return list;
        }

        /// <summary>
        /// 問い合わせ種別のタグを取得する。
        /// </summary>
        /// <param name="requestType">問い合わせ種別。</param>
        /// <returns>問い合わせ種別のタグ。</returns>
        static string GetRequestTypeTag(RequestType requestType)
        {
            switch (requestType)
            {
                case RequestType.Question:
                    return "操作や仕様質問";
                case RequestType.Bug:
                    return "不具合";
                case RequestType.Purchasing:
                    return "課金";
                case RequestType.Request:
                    return "要望";
                case RequestType.Other:
                    return "その他";
            }

            return string.Empty;
        }

        /// <summary>
        /// リクエストを送信する。
        /// </summary>
        /// <param name="requestType">問い合わせ種別。</param>
        /// <param name="platform">問い合わせをしているプラットフォーム。</param>
        /// <param name="emailAddress">連絡用のメールアドレス。</param>
        /// <param name="commentBody">問い合わせの本文。</param>
        public void SendRequest(RequestType requestType, RequesterPlatform platform, string emailAddress, string commentBody)
        {
            if (!IsReady)
            {
                return;
            }

            this.requestType = requestType;
            this.platform = platform;

            var request = new CreateRequest();
            request.Requester = new Requester();
            request.Requester.Email = emailAddress;
            request.CustomFields = CreateCustomFieldValues();
            request.Tags = new List<string>();
            request.Subject = $"【{appTitle}】{GetRequestTypeTag(requestType)}";
            var comment = new Comment();
            comment.Body = commentBody;
            request.Comment = comment;

            var wrapper = new CreateRequestWrapper();
            wrapper.Request = request;

            supportProvider.CreateRequest(OnRequestCreated, wrapper, null);
        }

        /// <summary>
        /// Zendeskのリクエスト送信後の処理。
        /// </summary>
        /// <param name="response">リクエスト送信処理のレスポンス。</param>
        void OnRequestCreated(ZendeskResponse<RequestResponse> response)
        {
            if (response.IsError)
            {
                ContactUsLogger.LogError($"Request Failed. ( {response.ErrorResponse.Reason} )\nResponseBodyType:{response.ErrorResponse.ResponseBodyType}\nResponseBody:{response.ErrorResponse.ResponseBody}");

                OnRequestFailed?.Invoke();
            }
            else
            {
                ContactUsLogger.Log($"Request Succeeded ! ( {response.Result.Request.Subject} )\n{response.Result.Request.Description}");

                OnRequestSucceeded?.Invoke();
            }
        }
    }
}